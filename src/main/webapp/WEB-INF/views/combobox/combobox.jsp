<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/commons/taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Employee</title>
	<link href='<c:url value="/css/bootstrap.min.css"/>' rel="stylesheet"></link>
	<script src='<c:url value="/js/jquery-3.1.0.min.js"/>'></script>
	<script src='<c:url value="/js/bootstrap.min.js"/>'></script>
</head>
<body>
<div class="container">
	<div class="page-header">
		<h3>Combobox</h3>
	</div>
	<form method="POST" class="form-inline">
		<div class="form-group">
			<label class="sr-only" for="companyId">Company</label>
			<div class="input-group">
				<div class="input-group-addon">Company</div>
				<form:select path="companies" id="companyId" name="companyId" cssClass="form-control">
					<form:option value="">請選擇...</form:option>
					<form:options items="${companies}" itemValue="id" itemLabel="name"/>
				</form:select>
			</div>
		</div>
		<div class="form-group">
			<label class="sr-only" for="employeeId">Employee</label>
			<div class="input-group">
				<div class="input-group-addon">Employee</div>
				<select id="employeeId" name="employeeId" class="form-control"></select>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
$(function(){
	$("#companyId").change(function() {
		var $that = $(this);
		if($that.val() != "") {
			$.ajax({
				url: '<c:url value="/mvc/combobox/company/"/>' + $that.val(),
				method: "GET",
				dataType: "json"
			}).done(function(data) {
				if(data && data.length > 0) {
					$("#employeeId").empty();
					for(var i = 0; i < data.length; i++) {
						$("#employeeId").append($("<option></option>").attr("value", data[i].id).text(data[i].name));
					}
				}
			});
		} else {
			$("#employeeId").empty();
		}
	});
});
</script>
</body>
</html>