<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/commons/taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Employee</title>
	<link href='<c:url value="/css/bootstrap.min.css"/>' rel="stylesheet"></link>
	<script src='<c:url value="/js/jquery-3.1.0.min.js"/>'></script>
	<script src='<c:url value="/js/bootstrap.min.js"/>'></script>
</head>
<body>
<div class="container">
	<div class="page-header">
		<h3>Combobox <small class="text-danger">With Details</small></h3>
	</div>
	<form method="POST" class="form-inline">
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-addon">Company</div>
				<select id="companyId" name="companyId" class="form-control">
					<option value="">----- Select -----</option>
					<c:forEach var="item" items="${companies}">
						<option value="${item.id}" <c:if test="${item.id eq companyId}">selected</c:if> >${item.name}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-addon">Employee</div>
				<select id="employeeId" name="employeeId" class="form-control">
					<c:forEach var="item" items="${employees}">
						<option value="${item.id}" <c:if test="${item.id eq employeeId}">selected</c:if> >${item.name}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<button type="submit" class="btn btn-primary">Search</button>
	</form>
	<c:if test="${not empty company}">
		<div class="page-header">
			<h3>Company</h3>
		</div>
		<table class="table table-bordered">
			<tr>
				<th class="text-right" style="width:120px;">ID</th>
				<td>${company.id}</td>
				<th class="text-right" style="width:120px;">Name</th>
				<td>${company.name}</td>
			</tr>
			<tr>
				<th class="text-right">Phone</th>
				<td>${company.phone}</td>
				<th class="text-right">Fax</th>
				<td>${company.fax}</td>
			</tr>
			<tr>
				<th class="text-right">Address</th>
				<td colspan="3">${company.address}</td>
			</tr>
		</table>
	</c:if>
	<c:if test="${not empty employee}">
		<div class="page-header">
			<h3>Employee</h3>
		</div>
		<table class="table table-bordered">
			<tr>
				<th class="text-right" style="width:120px;">ID</th>
				<td>${employee.id}</td>
				<th class="text-right" style="width:120px;">Name</th>
				<td>${employee.name}</td>
			</tr>
			<tr>
				<th class="text-right">Email</th>
				<td colspan="3">${employee.email}</td>
			</tr>
		</table>
	</c:if>
</div>
<script type="text/javascript">
$(function(){
	$("#companyId").change(function() {
		var $that = $(this);
		if($that.val() != "") {
			$.ajax({
				url: '<c:url value="/mvc/combobox/company/"/>' + $that.val(),
				method: "GET",
				dataType: "json"
			}).done(function(data) {
				if(data && data.length > 0) {
					$("#employeeId").empty();
					for(var i = 0; i < data.length; i++) {
						$("#employeeId").append($("<option></option>").attr("value", data[i].id).text(data[i].name));
					}
				}
			});
		} else {
			$("#employeeId").empty();
		}
	});
});
</script>
</body>
</html>