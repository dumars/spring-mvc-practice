<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/commons/taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Company List</title>
	<link href='<c:url value="/css/jquery-ui.min.css"/>' rel="stylesheet"></link>
	<link href='<c:url value="/css/bootstrap.min.css"/>' rel="stylesheet"></link>
	<script src='<c:url value="/js/jquery-3.1.0.min.js"/>'></script>
	<script src='<c:url value="/js/jquery-ui.min.js"/>'></script>
	<script src='<c:url value="/js/bootstrap.min.js"/>'></script>
</head>
<body>
<div class="container">
	<div class="page-header"><h3>Company Controller <small class="text-danger">jstl + jquery + bootstrap</small></h3></div>
	<div class="text-right"><button type="button" class="btn btn-default" onclick="createCompany()">Create Company</button></div>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Phone</th>
				<th>Fax</th>
				<th>Address</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${companies}" varStatus="state">
				<tr>
					<td>${item.name}</td>
					<td>${item.phone}</td>
					<td>${item.fax}</td>
					<td>${item.address}</td>
					<td style="max-width:80px;text-align:right;">
						<button type="button" class="btn btn-xs btn-primary" onclick="previewCompany(${item.id})">Preview</button>
						<button type="button" class="btn btn-xs btn-warning" onclick="editCompany(${item.id})">Edit</button>
						<button type="button" class="btn btn-xs btn-danger" onclick="deleteCompany(${item.id})">Delete</button>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<script type="text/javascript">
var uri = '<c:url value="/mvc/company/"/>';
function previewCompany(id) {
	window.location = uri + id;
}

function createCompany() {
	window.location = uri + "/creation";
}

function editCompany(id) {
	window.location = uri + id + "/modify";
}

function deleteCompany(id) {
	window.location = uri + id + "/delete";
}
</script>
</body>
</html>