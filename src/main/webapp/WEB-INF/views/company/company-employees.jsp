<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/commons/taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Company and Employee</title>
	<link href='<c:url value="/css/bootstrap.min.css"/>' rel="stylesheet"></link>
	<script src='<c:url value="/js/jquery-3.1.0.min.js"/>'></script>
	<script src='<c:url value="/js/bootstrap.min.js"/>'></script>
</head>
<body>
<div class="container">
	<div class="page-header"><h3>Company</h3></div>
	<div class="text-right" style="padding-bottom:20px;">
		<button type="button" id="back_btn" class="btn btn-warning">Back</button>
	</div>
	<table class="table table-bordered">
		<tr>
			<th class="text-right">ID</th>
			<td>${company.id}</td>
			<th class="text-right">Name</th>
			<td>${company.name}</td>
		</tr>
		<tr>
			<th class="text-right">Phone</th>
			<td>${company.phone}</td>
			<th class="text-right">Fax</th>
			<td>${company.fax}</td>
		</tr>
		<tr>
			<th class="text-right">Address</th>
			<td colspan="3">${company.address}</td>
		</tr>
	</table>
	<div class="page-header"><h3>Employees</h3></div>
	<div class="text-right" style="padding-bottom:20px;">
		<button type="button" class="btn btn-primary" onclick="createEmployee()">Create Employee</button>
	</div>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Email</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${employees}" varStatus="state">
				<tr>
					<td>${item.id}</td>
					<td>${item.name}</td>
					<td>${item.email}</td>
					<td style="max-width:80px;text-align:right;">
						<button type="button" class="btn btn-xs btn-primary" onclick="previewEmployee(${item.id})">Preview</button>
						<button type="button" class="btn btn-xs btn-warning" onclick="modifyEmployee(${item.id})">Edit</button>
						<button type="button" class="btn btn-xs btn-danger" onclick="deleteEmployee(${item.id})">Delete</button>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<script type="text/javascript">
var uri = '<c:url value="/mvc/company/"/>${company.id}/employee';
function previewEmployee(id) {
	window.location = uri + "/" + id + "/preview";
}

function createEmployee() {
	window.location = uri;
}

function modifyEmployee(id) {
	window.location = uri + "/" + id;
}

function deleteEmployee(id) {
	window.location = uri + "/" + id + "/delete";
}

$(function(){
	$("#back_btn").click(function() {
		window.location = '<c:url value="/mvc/companies"/>';
	});
});
</script>
</body>
</html>