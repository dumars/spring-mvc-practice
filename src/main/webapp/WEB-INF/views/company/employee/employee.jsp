<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/commons/taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Employee</title>
	<link href='<c:url value="/css/bootstrap.min.css"/>' rel="stylesheet"></link>
	<script src='<c:url value="/js/jquery-3.1.0.min.js"/>'></script>
	<script src='<c:url value="/js/bootstrap.min.js"/>'></script>
</head>
<body>
<div class="container">
	<div class="page-header"><h3>Employee</h3></div>
	<div class="text-right" style="padding-bottom:20px;">
		<button type="button" id="back_btn" class="btn btn-warning">Back</button>
	</div>
	<table class="table table-bordered">
		<tr>
			<th class="text-right" style="width:120px;">ID</th>
			<td>${employee.id}</td>
			<th class="text-right" style="width:120px;">Name</th>
			<td>${employee.name}</td>
		</tr>
		<tr>
			<th class="text-right">Email</th>
			<td colspan="3">${employee.email}</td>
		</tr>
	</table>
</div>
<script type="text/javascript">
$(function(){
	$("#back_btn").click(function() {
		window.location = '<c:url value="/mvc/company/"/>${employee.companyId}';
	});
});
</script>
</body>
</html>