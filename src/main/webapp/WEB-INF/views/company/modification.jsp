<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/commons/taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Company</title>
	<link href='<c:url value="/css/bootstrap.min.css"/>' rel="stylesheet"></link>
	<script src='<c:url value="/js/jquery-3.1.0.min.js"/>'></script>
	<script src='<c:url value="/js/bootstrap.min.js"/>'></script>
</head>
<body>
<div class="container">
	<div class="page-header"><h3>Company</h3></div>
	<form class="form-horizontal" method="POST" action='<c:url value="/mvc/company/"/>${company.id}'>
		<div class="text-right" style="padding-bottom:20px;">
			<button type="button" id="cancel_btn" class="btn btn-warning">Cancel</button>
			<button type="submit" class="btn btn-primary">Save</button>
		</div>
		<c:if test="${not empty error}">
			<div class="alert alert-warning alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<strong>Warning!</strong> ${error}
			</div>
		</c:if>
		<input type="hidden" name="id" value="${company.id}" class="form-control"/>
		<div class="form-group">
			<label class="col-sm-2 control-label">Name</label>
			<div class="col-sm-10"><input type="text" name="name" value="${company.name}" class="form-control"/></div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Phone</label>
			<div class="col-sm-10"><input type="text" name="phone" value="${company.phone}" class="form-control"/></div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Fax</label>
			<div class="col-sm-10"><input type="text" name="fax" value="${company.fax}" class="form-control"/></div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Address</label>
			<div class="col-sm-10"><input type="text" name="address" value="${company.address}" class="form-control"/></div>
		</div>
	</form>
</div>
<script type="text/javascript">
$(function(){
	$("#cancel_btn").click(function() {
		window.location = '<c:url value="/mvc/companies"/>';
	});
});
</script>
</body>
</html>