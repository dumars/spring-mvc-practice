<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/commons/taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Practice</title>
	<link href='<c:url value="/css/bootstrap.min.css"/>' rel="stylesheet"></link>
	<script src='<c:url value="/js/jquery-3.1.0.min.js"/>'></script>
	<script src='<c:url value="/js/bootstrap.min.js"/>'></script>
</head>
<body>
<div class="container">
	<div style="height:100px;">&nbsp;</div>
	<ul class="list-group">
		<li class="list-group-item"><a href="/practice/mvc/companies">Company and Employee</a></li>
		<li class="list-group-item"><a href="/practice/mvc/combobox">Double Layers Combobox</a></li>
		<li class="list-group-item"><a href="/practice/mvc/combobox/details">Double Layers Combobox with Details</a></li>
		<li class="list-group-item"><a href="/practice/mvc/file-upload">Single File Upload + RedirectAttributes</a></li>
		<li class="list-group-item"><a href="/practice/mvc/files-upload">Mutiple File Upload + RedirectAttributes</a></li>
	</ul>
</div>
</body>
</html>