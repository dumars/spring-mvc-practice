package org.bitbucket.dumars.practice.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.bitbucket.dumars.practice.entity.Entity;
import org.springframework.beans.BeanUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractBaseDao<T extends Entity> {

	protected List<T> objects;

	public List<T> findAll() {
		return objects;
	}

	public T findById(int id) {
		if (objects != null) {
			return objects.stream().filter(o -> o.getId() == id).findFirst().get();
		}
		return null;
	}

	public List<T> findIdBetween(int start, int end) {
		if (objects != null) {
			return objects.stream().filter(o -> o.getId() >= start && o.getId() <= end).collect(Collectors.toList());
		}
		return null;
	}

	public int insert(T t) {
		if (isExist(t)) {
			log.error("Duplicate id {}.", t.toString());
			return 0;
		} else {
			objects.add(t);
			return 1;
		}
	}

	public int update(T t) {
		T old = findById(t.getId());
		if (old != null) {
			BeanUtils.copyProperties(t, old);
			return 1;
		}
		return 0;
	}

	public int deleteById(int id) {
		T t = findById(id);
		if (t != null) {
			objects.remove(t);
			return 1;
		}
		return 0;
	}

	protected boolean isExist(T t) {
		if (objects != null) {
			return (objects.stream().anyMatch(com -> com.getId() == t.getId()));
		}
		return false;
	}
}
