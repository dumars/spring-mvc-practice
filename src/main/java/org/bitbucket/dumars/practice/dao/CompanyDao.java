package org.bitbucket.dumars.practice.dao;

import java.util.ArrayList;

import org.bitbucket.dumars.practice.entity.Company;
import org.springframework.stereotype.Repository;

@Repository
public class CompanyDao extends AbstractBaseDao<Company> {

	public CompanyDao() {
		objects = new ArrayList<Company>();
		for (int i = 0; i < 10; i++) {
			Company company = new Company();
			company.setId(i + 1);
			company.setName(String.format("No %d Company", i + 1));
			company.setPhone("8825252" + i);
			company.setFax("8825253" + i);
			company.setAddress("Monkey Rd., No. " + (i + 1));
			objects.add(company);
		}
	}
}
