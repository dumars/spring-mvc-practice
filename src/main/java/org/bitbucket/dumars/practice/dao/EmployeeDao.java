package org.bitbucket.dumars.practice.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bitbucket.dumars.practice.entity.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

@Repository
public class EmployeeDao extends AbstractBaseDao<Employee> {

	private final static Integer DEFAULT_EMPLOYEES = 5;

	public EmployeeDao() {
		objects = new ArrayList<Employee>();
		for (int i = 0; i < 10; i++) {
			for (int j = 1; j <= DEFAULT_EMPLOYEES; j++) {
				int id = i * DEFAULT_EMPLOYEES + j;
				Employee employee = new Employee();
				employee.setId(id);
				employee.setCompanyId(i + 1);
				employee.setEmail(String.format("%d@sample.org", id));
				employee.setName(String.format("User %d", id));
				objects.add(employee);
			}
		}
	}

	public List<Employee> findByCompanyId(int companyId) {
		if (objects != null) {
			return objects.stream().filter(employee -> employee.getCompanyId() == companyId)
					.collect(Collectors.toList());
		}
		return null;
	}

	public int deleteByCompanyId(int companyId) {
		List<Employee> employees = findByCompanyId(companyId);
		if (!CollectionUtils.isEmpty(employees)) {
			objects.removeAll(employees);
			return employees.size();
		}
		return 0;
	}
}
