package org.bitbucket.dumars.practice.controller.mvc;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.dumars.practice.dao.CompanyDao;
import org.bitbucket.dumars.practice.dao.EmployeeDao;
import org.bitbucket.dumars.practice.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class DoubleLayerComboboxController {

	@Autowired
	private CompanyDao companyDao;
	@Autowired
	private EmployeeDao employeeDao;

	@RequestMapping(value = "/combobox", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("companies", companyDao.findAll());
		model.addAttribute("employees", new ArrayList<Employee>());
		return "/combobox/combobox";
	}

	@ResponseBody
	@RequestMapping(value = "/combobox/company/{companyId}", method = RequestMethod.GET)
	public List<Employee> findEmployeesByCompanyId(@PathVariable Integer companyId) {
		return employeeDao.findByCompanyId(companyId);
	}

	@RequestMapping(value = "/combobox/details")
	public String comboboxWithDetails(Integer companyId, Integer employeeId, Model model) {
		log.debug("companyId: {}, employeeId: {}", companyId, employeeId);

		model.addAttribute("companies", companyDao.findAll());

		if (companyId != null) {
			model.addAttribute("company", companyDao.findById(companyId));
			model.addAttribute("employees", employeeDao.findByCompanyId(companyId));
		} else {
			model.addAttribute("employees", new ArrayList<Employee>());
		}

		if (employeeId != null) {
			model.addAttribute("employee", employeeDao.findById(employeeId));
		}

		model.addAttribute("companyId", companyId == null ? "" : companyId);
		model.addAttribute("employeeId", employeeId == null ? "" : employeeId);

		return "/combobox/combobox-details";
	}
}
