package org.bitbucket.dumars.practice.controller.mvc;

import org.bitbucket.dumars.practice.dao.EmployeeDao;
import org.bitbucket.dumars.practice.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EmployeeController {

	private final String REDIRECT_PARENT = "redirect:/mvc/company/%d";

	@Autowired
	private EmployeeDao employeeDao;

	@RequestMapping(value = "/company/{companyId}/employee/{id}/preview", method = RequestMethod.GET)
	public String preview(@PathVariable Integer companyId, @PathVariable Integer id, Model model) {
		Employee employee = employeeDao.findById(id);
		if (employee == null) {
			return String.format(REDIRECT_PARENT, companyId);
		}

		model.addAttribute("employee", employee);
		return "/company/employee/employee";
	}

	@RequestMapping(value = "/company/{companyId}/employee", method = RequestMethod.GET)
	public String create(@PathVariable Integer companyId, Model model) {
		Employee employee = new Employee();
		employee.setCompanyId(companyId);
		model.addAttribute("employee", employee);
		model.addAttribute("companyId", companyId);
		return "/company/employee/creation";
	}

	@RequestMapping(value = "/company/{companyId}/employee", method = RequestMethod.POST)
	public String save(@PathVariable Integer companyId, Employee employee, Model model) {
		int result = employeeDao.insert(employee);
		if (result > 0) {
			return String.format(REDIRECT_PARENT, companyId);
		}

		model.addAttribute("employee", employee);
		model.addAttribute("companyId", companyId);
		return "/company/employee/creation";
	}

	@RequestMapping(value = "/company/{companyId}/employee/{id}", method = RequestMethod.GET)
	public String modify(@PathVariable Integer companyId, @PathVariable Integer id, Model model) {
		Employee employee = employeeDao.findById(id);
		if (employee == null) {
			return String.format(REDIRECT_PARENT, companyId);
		}

		model.addAttribute("employee", employee);
		return "/company/employee/modification";
	}

	@RequestMapping(value = "/company/{companyId}/employee/{id}", method = RequestMethod.POST)
	public String update(@PathVariable Integer companyId, @PathVariable Integer id, Employee employee, Model model) {
		int result = employeeDao.update(employee);
		if (result > 0) {
			return String.format(REDIRECT_PARENT, companyId);
		}

		model.addAttribute("employee", employee);
		return "/company/employee/modification";
	}

	@RequestMapping(value = "/company/{companyId}/employee/{id}/delete", method = RequestMethod.GET)
	public String delete(@PathVariable Integer companyId, @PathVariable Integer id) {
		employeeDao.deleteById(id);
		return String.format(REDIRECT_PARENT, companyId);
	}
}
