package org.bitbucket.dumars.practice.controller.mvc;

import org.bitbucket.dumars.practice.dao.CompanyDao;
import org.bitbucket.dumars.practice.dao.EmployeeDao;
import org.bitbucket.dumars.practice.entity.Company;
import org.bitbucket.dumars.practice.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Dumars
 */
@Controller
public class CompanyController {

	@Autowired
	private CompanyDao companyDao;
	@Autowired
	private EmployeeDao employeeDao;
	@Autowired
	private CompanyService companyService;

	@RequestMapping(value = "/companies", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("companies", companyDao.findAll());
		return "/company/companies";
	}

	@RequestMapping(value = "/company/{id}", method = RequestMethod.GET)
	public String preview(@PathVariable Integer id, Model model) {
		Company company = companyDao.findById(id);
		if (company == null) {
			return "redirect:/mvc/companies";
		}

		model.addAttribute("company", company);
		model.addAttribute("employees", employeeDao.findByCompanyId(id));
		return "/company/company-employees";
	}

	@RequestMapping(value = "/company/{id}/modify", method = RequestMethod.GET)
	public String modify(@PathVariable Integer id, Model model) {
		Company company = companyDao.findById(id);
		if (company == null) {
			return "redirect:/mvc/companies";
		}

		model.addAttribute("company", company);
		return "/company/modification";
	}

	@RequestMapping(value = "/company/creation", method = RequestMethod.GET)
	public String create(Model model) {
		model.addAttribute("company", new Company());
		return "/company/creation";
	}

	@RequestMapping(value = "/company", method = RequestMethod.POST)
	public String insert(Company company, Model model) {
		int result = companyDao.insert(company);
		if (result > 0) {
			return "redirect:/mvc/companies";
		}

		model.addAttribute("company", company);
		model.addAttribute("error", "Insert failed.");
		return "/company/company";
	}

	@RequestMapping(value = "/company/{id}", method = RequestMethod.POST)
	public String update(@PathVariable Integer id, Company company, Model model) {
		int result = companyDao.update(company);
		if (result > 0) {
			return "redirect:/mvc/companies";
		}

		model.addAttribute("company", company);
		return "/company/company";
	}

	@RequestMapping(value = "/company/{id}/delete", method = RequestMethod.GET)
	public String delete(@PathVariable Integer id) {
		companyService.deleteCompanyById(id);
		return "redirect:/mvc/companies";
	}
}
