package org.bitbucket.dumars.practice.controller.mvc;

import java.io.File;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class FileUploadController {

	private final static String DEFAULT_STORED_PATH = "D:/";

	@RequestMapping(value = "/file-upload", method = RequestMethod.GET)
	public String index() {
		return "/file-upload/index";
	}

	@RequestMapping(value = "/file-upload", method = RequestMethod.POST)
	public String upload(@RequestParam("file") MultipartFile file, final RedirectAttributes redirectAttributes)
			throws Exception {
		if (!file.isEmpty()) {
			log.debug("The upload file name is {}", file.getOriginalFilename());
			file.transferTo(new File(DEFAULT_STORED_PATH, file.getOriginalFilename()));

			redirectAttributes.addFlashAttribute("message", "Good job!!");
		}
		return "redirect:/mvc/file-upload";
	}

	@RequestMapping(value = "/files-upload", method = RequestMethod.GET)
	public String multiFileUploadPage() {
		return "/file-upload/multiple";
	}

	@RequestMapping(value = "/files-upload", method = RequestMethod.POST)
	public String multipleUpload(@RequestParam("file") MultipartFile[] files,
			final RedirectAttributes redirectAttributes) throws Exception {
		if (files != null) {
			for (MultipartFile file : files) {
				log.debug("The upload file name is {}", file.getOriginalFilename());
				file.transferTo(new File(DEFAULT_STORED_PATH, file.getOriginalFilename()));
			}

			redirectAttributes.addFlashAttribute("message", "Goooooooooooooooood job!!");
		}
		return "redirect:/mvc/files-upload";
	}
}
