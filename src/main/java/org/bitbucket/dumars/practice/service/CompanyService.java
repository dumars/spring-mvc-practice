package org.bitbucket.dumars.practice.service;

import org.bitbucket.dumars.practice.dao.CompanyDao;
import org.bitbucket.dumars.practice.dao.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

	@Autowired
	private CompanyDao companyDao;
	@Autowired
	private EmployeeDao employeeDao;

	public int deleteCompanyById(int id) {
		int result = 0;
		result += companyDao.deleteById(id);
		result += employeeDao.deleteByCompanyId(id);
		return result;
	}
}
