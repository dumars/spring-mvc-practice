package org.bitbucket.dumars.practice.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Company implements Entity {
	@Getter @Setter private Integer id;
	@Getter @Setter private String name;
	@Getter @Setter private String phone;
	@Getter @Setter private String fax;
	@Getter @Setter private String address;
}
