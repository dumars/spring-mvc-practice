package org.bitbucket.dumars.practice.entity;

public interface Entity {

	public Integer getId();
	public void setId(Integer id);
}
