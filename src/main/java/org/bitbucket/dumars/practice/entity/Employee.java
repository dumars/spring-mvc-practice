package org.bitbucket.dumars.practice.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Employee implements Entity {
	@Getter @Setter private Integer id;
	@Getter @Setter private String name;
	@Getter @Setter private String email;
	@Getter @Setter private Integer companyId;
}
